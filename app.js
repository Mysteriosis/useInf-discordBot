
/*! UseInf-Discord-Chatbot
* https://github.com/Mysteriosis/useInf-discordBot
* Includes: discord.js, apiai
* Copyright 2018 P-Alain Curty; Licensed MIT */

require('dotenv').config()

const Discord = require('discord.js');
const Apiai = require('apiai');

const client = new Discord.Client();
const dialogflow = Apiai(process.env.APIAI_TOKEN);

// Log our bot in
client.login(process.env.DISCORD_TOKEN);

// Create an event listener for messages
client.on('message', message => {
    if((message.cleanContent.startsWith("@" + client.user.username) || message.channel.type == 'dm') && client.user.id != message.author.id) {
        const user = message.author;
        const msg = message.content.slice(message.content.indexOf(" ") + 1);

        // Call Dialogflow
        let request = dialogflow.textRequest(msg, {
            sessionId: user.id
        });

        // Create a promise to sync answer
        var promise = new Promise(function(resolve, reject) {
            request.on('response', function(response) {
                // console.log(response.result);
                // Prefer "speech" on displayText
                var res = response.result.fulfillment.speech;
                resolve(res);
            });
            
            request.on('error', function(error) {
                console.log(error);            
                resolve(null);
            });

            request.end();
        });

        // Wait for Dialogflow and process answer
        (async function() {
            var res = await promise;
            console.log(res);
            // If no answer, it's generally because Dialogflow, Python script or News API doesn't respond
            if(res) {
                // Try to parse JSON or else send raw text
                try {
                    let result = JSON.parse(res);
                    // Test for empty (correct) answers
                    if(result.articles.length > 0) {
                        let text = `I found some new about that:\n\n`;
                        result.articles.forEach(article => {
                            let date = new Date(article.date);
                            text += `${article.title} (${date.toLocaleString()}), publied from ${article.source}, at: ${article.url}\n\n`;
                        });

                        if(text.length > 2000)
                            message.reply(text.substring(0, 1995) + "[...]");
                        else
                            message.reply(text);
                    }
                    else
                        message.reply("Sorry, I didn't find anything...");
                }
                catch(error) {
                    console.error(error);
                    message.reply(res);
                }
            }
            else 
                message.reply("Sorry, I don't have access to my brain right now...");
        }());
    }
});

client.on('ready', () => {
    console.log('I am ready!');
});